<?php 

session_start();

class Login {
	public function login($email, $password) {
		$login = (object) [
			'email' => $email,
			'password' => $password
			'visibility' => "none"
		];

		if ($_SESSION['users'] === null) {
			$_SESSION['users'] = array();
		}

		array_push($_SESSION['users'], $login);
	}

	public function clear() {
		session_destroy();
	}
}


$auth = new Login();


if ($_POST['action'] === 'login') {
	if ($_POST['email'] == "johnsmith@gmail.com" && $_POST['password'] == "1234") {
		$auth->login($_POST['email'], $_POST['password']);
	}
	
} else if ($_POST['action'] === 'clear') {
	$auth->clear();
}


header('Location: ./index.php');


?>