<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>KT-PHP ACTIVITY 5</title>
</head>
<body>

	<?php session_start(); ?>


	<form method="POST" action="./server.php">
		<input type="hidden" name="action" value="login">
		Email: <input type="text" name="email" required>
		Password: <input type="password" name="password" required>

		<button type="submit">Login</button>
	</form>

	<?php if(isset($_SESSION['users'])): ?>
		<?php foreach ($_SESSION['users'] as $index => $user): ?>
			<form method="POST" action="./server.php">
				<input type="hidden" name="action" value="clear">
				<input type="hidden" name="id" value="<?= $index; ?>">
				<?= $user->email; ?> <br>
				<button>Logout</button>
			</form>
		<?php endforeach; ?>
	<?php endif; ?>

</body>
</html>